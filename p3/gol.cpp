/*mun
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */
string inpFile;


size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g);
void update();
int initFromFile(const char* fname);
void mainLoop();
void dumpState(FILE* f);

char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				inpFile = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	mainLoop();
	return 0;
}
const char ALIVE='0';
const char DEATH='.';

void display(vector< vector<char> > &world);
int width, length;
size_t nbrCount(size_t i,size_t j, const vector<vector<bool> >& g)
{
	size_t width = g[i].size();
	size_t length= g[j].size();

int main ()
{
vector< vector<char> >world(length, vector<char>(width, '.'));
vector< vector<char> >g(length,vector<char>(width, '.'));

while(true)
{
system("clear");
display(world);
sleep(10);
generation(world,g);

return 0;
}
}
void generation(vector< vector<char> >&world, vector<char> >&g);{
	if(g[i][j] == true)
	{
		int total = 0;
		if(g[i][(j-1+width)%width]== true)
			total++;
		if(g[i][(j+1+width)%width]== true)
			total++;
		if(g[(i-1+length)%length][j]== true)
			total++;
		if(g[(i+1+length)%length][j]== true)
			total++;
		if(g[(i-1+length)%length][(j-1+width)%width]== true)
			total++;
		if(g[(i+1+length)%length][(j-1+width)%width]== true)
			total++;
		if(g[(i-1+length)%length][(j+1+width)%width]== true)
			total++;
		if(g[(i+1+length)%length][(j+1+width)%width]== true)
			total++;

       if(total==2|| total=3)
	     return true;
	   else
		 return false ;

	}
	

	if(g[i][j] == false)
	{
		int total = 0;
		if(g[i][(j-1+width)%width]== true)
			total++;
		if(g[i][(j+1+width)%width]== true)
			total++;
		if(g[(i-1+length)%length][j]== true)
			total++;
		if(g[(i+1+length)%length][j]== true)
			total++;
		if(g[(i-1+length)%length][(j-1+width)%width]== true)
			total++;
		if(g[(i+1+length)%length][(j-1+width)%width]== true)
			total++;
		if(g[(i-1+length)%length][(j+1+width)%width]== true)
			total++;
		if(g[(i+1+length)%length][(j+1+width)%width]== true)
			total++;

		if (total== 3)
			return true;
		else
			return false;

		total = 0;
		// or these may able be combined, which is I think we can use "return total" instead of separate them into two situations.
		//switch(total){
		//case 3: if(total==3)
		//g[i][j]=true;
		//break;
		//case 2: if(total==2)
		//if(g[i][j]=true)
		//{
		//g[i][j]=true;
		//}
		//else
		//{
		//g[i][j]==false;
		//}
		//break;
		//default:
		//g[i][j]=false;
		//break;
		//return total;
		//this may be more clear because in the previous writing, not sure where does return true and return false go.
		// not sure whether switch works.
}
}


int initFromFile(const char* fname)
{
	FILE* f = fopen(fname,"rb");
	vector<vector<bool> > vec2;
	vector<bool> vec1;
	char c;
	while(fread(&c,1,1,f)!=0){
		if(c=='\n'){
		vec2.push_back(vec1);
		vec1.clear();
	}
		else{
			if(c=='.')
				vec1.push_back(false);
			else
				vec1.push_back(true);

		}
		}
	fclose(f);
	return 0;
	}

void dumpState(FILE* f, vector<vector<bool> > vec2, vector<bool> vec1)
{
	FILE* f= fopen("/tests/1","wb");
	char c;
	bool state;
	for(int j = 0; j< vec2.size(); j++){
			for(int i = 0; i < vec1.size(); i++){
				state = nbrCount(i,j, vec2);
				if(state ==true){
					c= 'o';
				}
				else{
					c = '.';
				}

			wfilename<<fwrite(&c,1,1,f);

	}
}
}



void mainLoop() {
	/* update, write, sleep */
	/*initial read from file given by user*/
	const* char ifile=inpFile.c_str();
	/*while(){ not sure where to end this but maybe when it gets to a
		//certain generation?*/
	initFromFile(ifile);
	ifile=initfilename.c_str();
	nbrCount();//put the vector we read from file through here
	dumpState();//write the new state to the tmp file
	//end while loop?
	system("clear");
	display(world);
	sleep(10); //time is various
	generation(world,g);
	
}

